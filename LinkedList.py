import time
import matplotlib.pyplot as plt

class Node:
    def __init__(self, data, next):
        self.data = data
        self.next = next

class Linked_List:
    def __init__(self):
        self.head = None

    def print(self):
        
        if self.head is None:
            print("Linked list is empty")
            return

        number = self.head
        linked_list_result = ''
        while number:
            linked_list_result += str(number.data)+' --> ' if number.next else str(number.data)
            number = number.next
        print(linked_list_result)

    def insert_at_begining(self,data):
        node = Node(data, self.head)
        self.head = node


    def get_length(self):
        count = 0
        number = self.head
        while number:
            count+=1
            number = number.next

        return count

    def insert_at_index(self,index,data):
        
        if index<0 or index>self.get_length():
            print ("Index not in range!")

        if index==0:
            self.insert_at_begining(data)
            return

        count = 0
        a = self.head
        while a:
            if count == index - 1:
                node = Node(data, a.next)
                a.next = node
                break    

ll= Linked_List()
ll.insert_at_begining(0)
results = []

list_length = [10,50,100,150,200,250,300,600,1000]
for l in list_length:
    numbers = range(l)
    for i in numbers:
        ll.insert_at_index(1,i)

    start=time.time()
    ll.insert_at_index(1,"insertion")
    stop=time.time()
    results.append(stop-start)

ll.print()
print(results)
plt.plot (list_length,results)
plt.ylim(0, 0.0002)
plt.show ()
